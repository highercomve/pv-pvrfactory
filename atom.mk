LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := trail
LOCAL_CATEGORY_PATH := system

TRAIL_SRC_DIR := $(LOCAL_PATH)

$(call load-config)

# Build
trail:
	@echo "Using pvr to create factory trail step"
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/objects
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/trails
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/vendor
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/boot
	@if [ -e $(TARGET_VENDOR_DIR)/addons ]; then \
		rm -f $(TARGET_OUT_TRAIL_STAGING)/addon-* $(TARGET_OUT_TRAIL_STAGING)/bsp/addon-*; \
		if ls $(TARGET_VENDOR_DIR)/addons/*.*z4 &> /dev/null; then \
			for a in $(TARGET_VENDOR_DIR)/addons/*z4; do \
				addon=`basename $$a`; \
				mkdir -p $(TARGET_OUT_TRAIL_STAGING)/bsp; \
				cp -f $$a $(TARGET_OUT_TRAIL_STAGING)/bsp/addon-$$addon; \
			done; \
		fi; \
	fi
	@if [ -e $(TARGET_VENDOR_DIR)/firmware ] && [ "$(PVR_USE_SRC_BSP)" != "yes" ]; then \
		rm $(TARGET_OUT_TRAIL_STAGING)/firmware.squashfs; \
		mksquashfs $(TARGET_VENDOR_DIR)/firmware $(TARGET_OUT_TRAIL_STAGING)/firmware.squashfs -b 1048576 -comp xz -Xdict-size 100%; \
	fi
	@echo "Setting TOP_DIR: $(TOP_DIR)"
	@echo "Installing BSP modules"
	@if [ -d $(TARGET_OUT_STAGING)/lib/modules/*/build ]; then \
		rm -rf $(TARGET_OUT_STAGING)/lib/modules/*/source; \
		rm -rf $(TARGET_OUT_STAGING)/lib/modules/*/build; \
		mksquashfs $(TARGET_OUT_STAGING)/lib/modules/* $(TARGET_OUT_TRAIL_STAGING)/modules.squashfs -all-root -comp xz && rm -rf $(TARGET_OUT_STAGING)/lib/modules/*; \
	fi
	@echo "Addons: `ls $(TARGET_VENDOR_DIR)/addons/*.*z4 ls $(TARGET_OUT_TRAIL_STAGING)/bsp/addon-*.*z4 2>/dev/null`"
	@TOP_DIR=$(TOP_DIR) PV_ADDONS="`ls $(TARGET_OUT_TRAIL_STAGING)/addon-*.*z4 $(TARGET_OUT_TRAIL_STAGING)/bsp/addon-*.*z4 2>/dev/null`" FDT_DTB=`basename $(TARGET_LINUX_DEVICE_TREE)` fakeroot $(TRAIL_SRC_DIR)/pvrfactory $(TARGET_OUT_TRAIL_STAGING) $(TARGET_OUT_TRAIL_FINAL) user1 user1
	@mkdir -p $(TARGET_OUT_TRAIL_FINAL)/boot/
	@mkdir -p $(TARGET_OUT_TRAIL_FINAL)/vendor/
	@echo -ne "pv_rev=0\0" > $(TARGET_OUT_TRAIL_FINAL)/boot/uboot.txt
	@if test ! -z "$(TARGET_FDT_FILE)"; then echo -ne "pv_fdtfile=$(TARGET_FDT_FILE)\0" >> $(TARGET_OUT_TRAIL_FINAL)/boot/uboot.txt; fi
	@mkdir -p $(TARGET_OUT_TRAIL_FINAL)/config/
	@if [ ! -e $(TARGET_CONFIG_DIR)/pantahub.config ]; then echo "ERROR: No pantahub.config file in $(TARGET_CONFIG_DIR), must exist before building trail"; exit 1; fi
	@cp $(TARGET_CONFIG_DIR)/pantahub.config $(TARGET_OUT_TRAIL_FINAL)/config/pantahub.config

include $(BUILD_CUSTOM)
